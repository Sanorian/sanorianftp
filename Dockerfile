FROM python:latest
WORKDIR /ftp
COPY . .
RUN pip install -r requirements.txt
EXPOSE 80
RUN python script.py
CMD ["python", "-m", "hypercorn", "ftp:app", "--bind", "0.0.0.0:80"]