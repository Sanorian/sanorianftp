# import fastapi library
from fastapi import FastAPI
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware
# import os library for
from os.path import exists

app = FastAPI()

# Adding CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

# get request for the html page
@app.get('/')
async def main():
    try:
        if exists('index.html'):
            return FileResponse('index.html')
        else:
            return
    except Exception as e:
        print(e)
        return FileResponse('error.html')

# get request for the favicon.ico
@app.get('/favicon.ico')
async def ico():
    if exists('favicon.ico'):
        return FileResponse('favicon.ico')
    else:
        return

# get request for the archive
@app.get('/archive')
async def archive():
    try:
        if exists('files.zip'):
            return FileResponse('files.zip')
        else:
            return FileResponse('error.html')
    except Exception as e:
        print(e)
        return FileResponse('error.html')

# get request for getting the file by its name
@app.get('/get/{file}')
async def get_file(file: str):
    try:
        if exists(f'files/{file}'):
            return FileResponse(f'files/{file}')
        else:
            return FileResponse('error.html')
    except Exception as e:
        print(e)