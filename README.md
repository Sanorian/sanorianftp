# SanorianFTP:
## Python script for creating FTP server using Python FastAPI
## Upload all files to ```/files``` directory
# Run:
```
python script.py
pip install -r requirements.txt
python -m hypercorn ftp:app --bind 0.0.0.0:80
```
## OR
```
docker-compose up
```