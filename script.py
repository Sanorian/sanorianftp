# import os library
import os
from os.path import isfile, join
# import zipfile library for creating zipfile
from zipfile import ZipFile

try:
    # get all filenames from /files directory
    files = [f for f in os.listdir('files') if isfile(join('files', f))]
    # initialize the content base of the html page
    inner = '<DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>SanorianFTP</title></head><body><h2>Archive of all files:</h2><a href="/archive" download>Download archive</a>'
    # initialize archive zip file
    with ZipFile("files.zip", "w") as zf:
        # iteration over file names
        for file in files:
            # adding files to archive
            zf.write(f"files/{file}")
            # renaming files
            new_name = file.replace(" ", "_")
            os.rename(f"files/{file}", f'files/{new_name}')
            # adding files links to the content of the html page
            inner += f'<h2>{new_name}</h2><a href="/get/{new_name}" download>Download {new_name}</a><h6></h6><a href="/get/{new_name}">Show {new_name}</a>'
    # ending creating contents of html page
    inner += '</body></html>'
    # creating a file and writing the content to the html page file
    with open("index.html", 'w+') as html_file:
        html_file.write(inner)
except Exception as e:
    print(e)